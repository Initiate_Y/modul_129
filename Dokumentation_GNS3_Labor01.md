# Dokumentation Übung - Mein PC pingt ins Labor

 - Datum: 26.11.2021
 - Name: Dylan Nideröst
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/20_GNS3%20Einf%C3%BChrung)

![GNS3 Screenshot meines Labors (Labor 1)](LABOR1.PNG)

## Switch 1
 - Switch e0 verbunden mit VPC e0
 - Switch e1 verbunden mit VPC e0
 - Beide VPC's können angepingt werden.

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.1.2 255.255.255.0
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.1.3 255.255.255.0
```

## Config Switch1
- [GNS3 Ethernet Switch](https://docs.gn3.com/search?q=switch)
- 2x Ports belegt

## Quellen
 -  https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/20_GNS3%20Einf%C3%BChrung

## Neue Lerninhalte
 - GNS3 Einrichten und Kennenlernen, war für mich bis jetzt komplett neu
 - Dokumentation mit Markdown erstellen, ebenfalls noch nie gemacht
 - VPCs mit IP Adresse und Subnetzmaske Konfigurieren und in betrieb nehmen

## Reflexion
Ich hatte zuerst einige Probleme bei der Übung, da ich mehrmals meinen VPC nicht anpingen konnte, obwohl ich dem Erklärvideo ziemlich genau gefolgt bin. Ich habe deshalb alle Schritte nochmals wiederholt, bis es schlussendlich dann doch funktioniert hat. Auch wenn ich bereits Erfahrungen mit gewissen Switches aus dem Betrieb hatte, war es doch ziemlich anspruchsvoll zu verstehen, was ich jetzt denn genau gemacht habe. Doch wenn man sich ein wenig Mühe gibt die Aufgabe richtig zu verstehen, lernt man aus den Fehlern...

