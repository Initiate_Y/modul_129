
# Dokumentation Labor 4 - Aggregierte statische Routen

 - Datum: 04.02.2022
 - Name: Dylan Nideröst
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LABOR4.PNG)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.22.1/24 interface=ether2
/ip/address add address=192.168.255.1/30 interface=ether3

/ip/route/ add dst-address=192.168.128.0/17 gateway=192.168.255.2
```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.255.2/30 interface=ether3
/ip/address add address=192.168.146.1/24 interface=ether2
/ip/address add address=192.168.255.5/30 interface=ether4

/ip/route/ add dst-address=192.168.210.0/24 gateway=192.168.255.6
/ip/route/ add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route/ add dst-address=192.168.22.0/24 gateway=192.168.255.1
```

## Config R3
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R3
/ip/address add address=192.168.255.6/30 interface=ether4
/ip/address add address=192.168.210.1/24 interface=ether2

/ip/route/ add dst-address=192.168.0.0/16 gateway=192.168.255.5
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.22.2 255.255.255.0 192.168.22.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.146.2 255.255.255.0 192.168.146.1
```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC3
# [IP] [MASKE] [GATEWAY]
ip 192.168.210.2 255.255.255.0 192.168.210.1
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.0.0 mask 255.255.0.0 192.168.23.138
```

## Quellen
 - https://www.youtube.com/watch?v=QqEcCzhlWis
 - https://www.youtube.com/watch?v=N54mZ5DDqYw

## Neue Lerninhalte
 - Aggregierte Route kennengelernt

## Reflexion
Ich hatte zuerst keine Ahnung von Aggregierten Routen und musste erstmal den Begriff googeln. Damit werden scheinbar die Anzahl Routing Tabellen im Netzwerk minimiert.
So kam ich auch auf die Lösung, dass ich das kleinste mögliche Netz von beiden Netzen A und C für die Aggregierte Route zwischen Netz B und D benötigte.
Danach musste ich mir noch ein wenig Hilfe von den Kollegen hohlen, da ich nicht wusste, was ich beim Routing falsch machte.
Ich bin froh dass ich diese Aufgabe mit relativ wenig Hilfe geschafft habe...