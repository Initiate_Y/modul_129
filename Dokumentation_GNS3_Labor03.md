
# Dokumentation Labor 3 - Ping über Router (3 Subnetze) und lokalem PC

 - Datum: 28.01.2022
 - Name: Dylan Nideröst
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LABOR3.PNG)

## Cloud
 - br0 192.168.23.0/24
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.25.1/24 interface=ether2
/ip/address add address=192.168.255.1/30 interface=ether3

/ip/route add dst-address=192.168.24.0/24 gateway=192.168.255.2
```


## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 2 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.24.1/24 interface=ether2
/ip/address add address=192.168.255.2/30 interface=ether3

/ip/route add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route add dst-address=192.168.25.0/24 gateway=192.168.255.1
```


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.25.2 255.255.255.0 192.168.25.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.24.2 255.255.255.0 192.168.24.1
```


## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.25.0 mask 255.255.255.0 192.168.23.135
route add 192.168.24.0 mask 255.255.255.0 192.168.23.135
route add 192.168.24.0 mask 255.255.255.0 192.168.255.2
```

## Quellen
 - https://wiki.mikrotik.com/wiki/Manual:Simple_Static_Routing
 - https://forum.mikrotik.com/viewtopic.php?t=110576
 - https://www.youtube.com/watch?v=ZYeMuYBAVrQ

## Neue Lerninhalte
 - Lokale Route auf auf einem Windows-PC konfigurieren
 - Routen mit Mikrotik hinzufügen

## Reflexion
Ich habe diese Aufgabe ohne wirklichen Plan angefangen und hatte nicht wirklich eine Ahnung wie IP-Routen funktionierten. Ich musste Hilfe vom Internet und Kollegen erhalten,
sowie den Lehrer fragen, um das Routing zu verstehen. Wir haben dann das Beispiel mit den verschiedenen Wegweisern und Strassennamen verwendet, dann konnte ich es besser verstehen!
