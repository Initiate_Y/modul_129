# Dokumentation Labor 5 - Labor mit 2 Router und DHCP Server

 - Datum: 04.02.2022
 - Name: Dylan Nideröst
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](LABOR5.PNG)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 4 Interfaces Enabled
```
/system/identity set name=R1

/interface/bridge/ add name=bridge1
/interface/bridge/port/ add bridge=bridge1 interface=ether6
/interface/bridge/port/ add bridge=bridge1 interface=ether7

/ip/pool add name=dhcp_pool1 ranges=192.168.45.10-192.168.45.254
/ip/dhcp-server/ add address-pool=dhcp_pool1 interface=bridge1 name=dhcp1
/ip/dhcp-server/network add address=192.168.45.0/24 gateway=192.168.45.1

/ip/address/ add address=192.168.45.1/24 interface=bridge1
/ip/address/ add address=192.168.255.1/30 interface=ether3

/ip/route/ add dst-address=192.168.46.0/24 gateway=192.168.255.2

/ip/firewall/filter add chain=forward src-address=192.168.23.0/24 dst-address=192.168.45.0/24 protocol=icmp action=drop
```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 4 Interfaces Enabled
```
/system/identity set name=R2

/interface/bridge/ add name=bridge2
/interface/bridge/port/ add bridge=bridge2 interface=ether6
/interface/bridge/port/ add bridge=bridge2 interface=ether7

/ip/pool add name=dhcp_pool2 ranges=192.168.46.10-192.168.46.254
/ip/dhcp-server/ add address-pool=dhcp_pool2 interface=bridge2 name=dhcp2
/ip/dhcp-server/network add address=192.168.46.0/24 gateway=192.168.46.1

/ip/address/ add address=192.168.46.1/24 interface=bridge2
/ip/address/ add address=192.168.255.2/30 interface=ether3

/ip/route/ add dst-address=192.168.45.0/24 gateway=192.168.255.1
```


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Config VPC 4
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.45.0 mask 255.255.255.0 192.168.23.140
route add 192.168.46.0 mask 255.255.255.0 192.168.23.139
```

## Quellen
 - https://help.mikrotik.com/docs/
 - https://wiki.mikrotik.com/wiki/
 - https://youtube.com/watch?v=NSDAYnixdgc

## Neue Lerninhalte
 - MikroTik DHCP 
 - MikroTik Firewall
 - Firewall Filter
 - Bridges

## Reflexion
Ich habe diesen Auftrag mit viel Hilfe der Kollegen abschliessen können. Zuerst musste ich aber auch mehrere Male googeln, wie die Konfigurationen aussehen und habe
Tim Schefer gefragt, der das Labor schon beendet hatte.
