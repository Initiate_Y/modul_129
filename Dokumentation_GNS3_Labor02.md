
# Dokumentation Labor2 - Ping mit Router

 - Datum: 28.01.2022
 - Name: Dylan Nideröst
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors (Labor 2)](LABOR2.PNG) 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 2 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.1.1/24 interface=ether1
/ip/address add address=192.168.2.1/24 interface=ether2
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.1.2 255.255.255.0 192.168.1.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.2.3 255.255.255.0 192.168.2.1
```


## Quellen
 - https://wiki.mikrotik.com/wiki/Manual:IP/Address#Example
 - Compendio

## Neue Lerninhalte
 - Hatte schon Erfahrungen mit Mikrotiktok-Router, da ich Labor 3 bereits abgeschlossen hatte.
 - Mehrere Interfaces mit entsprechender IP und Netzwerk auf Router konfigurieren
 - Nicht vergessen CIDR z.B. /24 immer beim hinzufügen der IP-Adresse hinschreiben.

## Reflexion
Ich hatte grundsätzlich keine Probleme, da ich bereits verstand wie das ganze Routing zwischen zwei PC's funktioniert.
Jedoch hat es zu Anfang nicht richtig funktioniert und dies lag an der CIDR die ich nicht eingefügt hatte, weshalb er mir deshalb das Netz nicht finden konnte...
